// <script src="./baitap.js"></script>

function xuly() {
  var chuan = document.getElementById("chuan").value * 1;
  var monthunhat = document.getElementById("mon1").value * 1;

  var monthuhai = document.getElementById("mon2").value * 1;
  var monthuba = document.getElementById("mon3").value * 1;
  var diemuukv = document.getElementById("uukhuvuc").value * 1;
  var diemuudt = document.getElementById("uudoituong").value * 1;

  var tongdiem = monthunhat + monthuhai + monthuba + diemuudt + diemuukv;
  //  ko có * 1 thì chỉ so sánh số đầu tiên

  if (monthunhat > 0 && monthuhai > 0 && monthuba > 0 && tongdiem >= chuan) {
    document.getElementById(
      "bai1"
    ).innerHTML = ` chúc mừng bạn đã đậu với số điểm ${tongdiem}`;
  } else {
    document.getElementById("bai1").innerHTML = ` bạn đã trượt kỳ thi`;
  }

  // bài 2
  var thongtin = document.getElementById("thongtin").value;
  var sodiensd = document.getElementById("sodien").value * 1;
  var tongtiendien = 0;

  // hoặc sử dụng switch
  //  switch case() la kiểu số--> biến banlaai phải *1
  if (sodiensd <= 50) {
    tongtiendien = sodiensd * 500;
    document.getElementById(
      "bai2"
    ).innerText = `  chào ${thongtin} số tiền bạn phải đóng là ${tongtiendien}`;
  } else if (sodiensd > 50 && sodiensd <= 100) {
    tongtiendien = 50 * 500 + (sodiensd - 50) * 650;
    document.getElementById(
      "bai2"
    ).innerText = `  chào ${thongtin} số tiền bạn phải đóng là ${tongtiendien}`;
  } else if (sodiensd > 100 && sodiensd <= 200) {
    tongtiendien = 50 * 500 + 50 * 650 + (sodiensd - 100) * 850;
    document.getElementById(
      "bai2"
    ).innerText = `  chào ${thongtin} số tiền bạn phải đóng là ${tongtiendien}`;
  } else if (sodiensd > 200 && sodiensd <= 350) {
    tongtiendien = 50 * 500 + 50 * 650 + 850 * 100 + (sodiensd - 200) * 1100;
    document.getElementById(
      "bai2"
    ).innerText = `  chào ${thongtin} số tiền bạn phải đóng là ${tongtiendien}`;
  } else {
    tongtiendien =
      50 * 500 + 50 * 650 + 850 * 100 + 150 * 1100 + (sodiensd - 350) * 1300;
    document.getElementById(
      "bai2"
    ).innerText = `  chào ${thongtin} số tiền bạn phải đóng là ${tongtiendien}`;
  }

  //bài 3
  var thongtincanhan = document.getElementById("ttcanhan").value;
  var npthuoc = document.getElementById("nguoiphuthuoc").value * 1;
  var luongmotnam = document.getElementById("tongthu").value * 1;
  var thunhapchiuthue = luongmotnam - 4 - npthuoc * 1.6;
  var thuephaidong = 0;
  if (thunhapchiuthue <= 60) {
    thuephaidong = (thunhapchiuthue * 5) / 100;
  } else if (thunhapchiuthue > 60 && thunhapchiuthue <= 120) {
    thuephaidong = (60 * 5) / 100 + ((thuephaidong - 60) * 10) / 100;
  } else if (thunhapchiuthue > 120 && thunhapchiuthue <= 210) {
    thuephaidong =
      (60 * 5) / 100 + (60 * 10) / 100 + ((thunhapchiuthue - 120) * 15) / 100;
  } else if (thunhapchiuthue > 210 && thunhapchiuthue <= 384) {
    thuephaidong =
      (60 * 5) / 100 +
      (60 * 10) / 100 +
      (90 * 15) / 100 +
      ((thunhapchiuthue - 210) * 20) / 100;
  } else if (thunhapchiuthue > 384 && thunhapchiuthue <= 624) {
    thuephaidong =
      (60 * 5) / 100 +
      (60 * 10) / 100 +
      (90 * 15) / 100 +
      (174 * 20) / 100 +
      ((thunhapchiuthue - 384) * 25) / 100;
  } else if (thunhapchiuthue > 624 && thunhapchiuthue <= 960) {
    thuephaidong =
      (60 * 5) / 100 +
      (60 * 10) / 100 +
      (90 * 15) / 100 +
      (174 * 20) / 100 +
      (240 * 25) / 100 +
      ((thunhapchiuthue - 624) * 30) / 100;
  } else {
    thuephaidong =
      (60 * 5) / 100 +
      (60 * 10) / 100 +
      (90 * 15) / 100 +
      (174 * 20) / 100 +
      (240 * 25) / 100 +
      (336 * 30) / 100 +
      ((thunhapchiuthue - 960) * 35) / 100;
  }

  // console.log("nhan",nhan);
  document.getElementById(
    "bai3"
  ).innerHTML = `chào ${thongtincanhan} thuế thu nhập của bạn là ${thuephaidong}  `;

  //bài 4
  var canh1 = document.getElementById("c1").value * 1;
  var canh2 = document.getElementById("c2").value * 1;
  var canh3 = document.getElementById("c3").value * 1;
  if (canh1 == canh2 && canh2 == canh3) {
    document.getElementById("bai4").innerHTML = `  tam giác đều`;
  } else if (canh1 == canh2 || canh1 == canh3 || canh3 == canh2) {
    document.getElementById("bai4").innerHTML = `  tam giác cân`;
  } else if (
    canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
    canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
    canh3 * canh3 == canh2 * canh2 + canh1 * canh1
  ) {
    document.getElementById("bai4").innerHTML = `  tam giác vuông`;
  } else {
    document.getElementById("bai4").innerHTML = `  tam giác thường`;
  }

  //bài 5
  var chuso = document.getElementById("socohaichuso").value * 1;
  var so_hang_dv = chuso % 10;
  var so_hang_chuc = (chuso - so_hang_dv) / 10;
  var tongky = so_hang_dv + so_hang_chuc;
  // document.getElementById("bai5dv").innerHTML = so_hang_dv;
  // document.getElementById("bai5chuc").innerHTML = so_hang_chuc;
  document.getElementById("bai5tongky").innerHTML = tongky;
  // console.log("tongky",tongky);
}
